# Automation Project on Protractor

Testing examples for https://the-internet.herokuapp.com/ website.

## Prerequisites
Following software should be installed:

* ```NodeJS | version 12+```
* ```Java JDK | version 8+```

## Usage

### Install Packages
* ```npm i```

### Install Protractor and WebDriver-Manager globally
* ```npm i -g protractor```

### Update drivers
* ```webdriver-manager update```

### Run Webdriver-Manager
* ```webdriver-manager start```

### Run Tests
Run the tests by using one of the following commands:

* ```npm run test```
* ```protractor config.js```

## Reports
Report is placed to ```report``` folder with ```.html``` format.
Screenshots are captured only for failed tests.
Report files are recreated every time you run tests.