const UploadPage = require('../pages/upload.page');

describe('Upload Page', () => {
  let page;

  beforeEach(() => {
    page = new UploadPage().get();
  });

  it('should display error message if no file selected for upload', () => {
    page.clickUploadBtn();

    expect(page.getErrorMsgTxt()).toEqual('Internal Server Error');
  });
  
  it('should upload file', () => {
    const file = 'upload.txt';
    
    page.uploadFile(file);

    expect(page.getUploadedTitleTxt()).toEqual('File Uploaded!');
    expect(page.getUploadedFileMsg()).toEqual(file);
  });

  it('should upload multiple files', () => {
    const file1 = 'upload.txt';
    const file2 = 'upload2.txt';

    page.uploadMultipleFiles([file1, file2]);

    expect(page.uploadedFiles.count()).toBe(2);
    expect(page.uploadedFiles.get(0).getText()).toEqual(file1);
    expect(page.uploadedFiles.get(1).getText()).toEqual(file2);
    expect(page.successMarks.count()).toBe(2);
  });

});