const NestedFramesPage = require('../pages/nested-frames.page');
const testData = require('../data/nested-frames.data.json');
const using = require('jasmine-data-provider');

describe('Nested Frames page', () => {
  let page;

  beforeEach(() => {
    page = new NestedFramesPage().get();
  });

  using(testData, data => {
    it(`should have ${data.text} frame`, () => {
      page.switchToFrames(data.selectors);
      expect(page.getBodyTxt()).toEqual(data.text);
    });
  });

});
