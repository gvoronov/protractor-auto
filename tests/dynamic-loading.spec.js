const DynamicLoadingPage = require('../pages/dynamic-loading.page');

describe('Dynamic Loading page', () => {
  let page;

  beforeEach(() => {
    page = new DynamicLoadingPage().get();
  });

  it('should display hidden element after loading', () => {
    page
      .goToFirstExample()
      .clickStartBtn();

    expect(page.getResultTxt()).toEqual('Hello World!');
    expect(page.result.isDisplayed()).toBe(true);
  });

  it('should render new element after loading', () => {
    page
      .goToSecondExample()
      .clickStartBtn();

    expect(page.getResultTxt()).toEqual('Hello World!');
    expect(page.result.isDisplayed()).toBe(true);
  });

});