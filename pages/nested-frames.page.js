const BasePage = require('../pages/base.page');

class NestedFramesPage extends BasePage {
  body = $('body');

  get() {
    browser.get('https://the-internet.herokuapp.com/nested_frames');
    return this;
  }

  getBodyTxt() {
    this.waitVisibilityOf(this.body, 5000);
    return this.body.getText();
  }

  switchToFrames(selectors) {
    selectors.forEach(selector => {
      let frame = element(by.name(selector));
      browser.switchTo().frame(frame.getWebElement());
    });
    return this;
  }
}

module.exports = NestedFramesPage;