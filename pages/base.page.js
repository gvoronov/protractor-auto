// Parent class, used for helper methods.
class BasePage {
  EC = protractor.ExpectedConditions;

  waitVisibilityOf(element, timeout) {
    browser.wait(this.EC.visibilityOf(element), timeout);
  }

  waitInvisibilityOf(element, timeout) {
    browser.wait(this.EC.invisibilityOf(element), timeout);
  }

};

module.exports = BasePage;