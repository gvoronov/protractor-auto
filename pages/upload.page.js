const BasePage = require('../pages/base.page');
const path = require('path');

class UploadPage extends BasePage {
  uploadField = $('#file-upload');
  uploadBtn = $('#file-submit');
  uploadedFileMsg = $('#uploaded-files');
  uploadedTitle = $('#content h3');

  multipleUploadField = $('body > input[type="file"]');
  uploadedFiles = $$('#drag-drop-upload .dz-filename');
  successMarks = $$('#drag-drop-upload .dz-success-mark');

  errorMsg = $('body > h1');

  get() {
    browser.get('https://the-internet.herokuapp.com/upload');
    return this;
  }

  clickUploadBtn() {
    this.uploadBtn.click();
    return this;
  }

  uploadFile(file) {
    let absolutePath = path.resolve(__dirname, '../data/' + file);
    this.uploadField.sendKeys(absolutePath);
    this.clickUploadBtn();
    this.waitVisibilityOf(this.uploadedFileMsg, 5000);
    return this;
  }

  uploadMultipleFiles(files) {
    files.forEach(file => {
      let absolutePath = path.resolve(__dirname, '../data/' + file);
      this.multipleUploadField.sendKeys(absolutePath);
    });
    return this;
  }

  getErrorMsgTxt() {
    this.waitVisibilityOf(this.errorMsg, 5000);
    return this.errorMsg.getText();
  }

  getUploadedTitleTxt() {
    this.waitVisibilityOf(this.uploadedTitle, 5000);
    return this.uploadedTitle.getText();
  }

  getUploadedFileMsg() {
    this.waitVisibilityOf(this.uploadedFileMsg, 5000);
    return this.uploadedFileMsg.getText();
  }

}

module.exports = UploadPage;