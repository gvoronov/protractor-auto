const BasePage = require('../pages/base.page');

class DynamicLoadingPage extends BasePage {
  firstExampleLink = $('#content a:nth-child(5)');
  secondExampleLink = $('#content a:nth-child(8)');

  startBtn = $('#start > button');
  loading = $('#loading');
  result = $('#finish > h4');

  get() {
    browser.get('https://the-internet.herokuapp.com/dynamic_loading');
    return this;
  }

  getResultTxt() {
    this.waitVisibilityOf(this.result, 10000);
    return this.result.getText();
  }

  goToFirstExample() {
    this.firstExampleLink.click();
    return this;
  }

  goToSecondExample() {
    this.secondExampleLink.click();
    return this;
  }

  clickStartBtn() {
    this.startBtn.click();
    this.waitVisibilityOf(this.loading, 5000);
    return this;
  }
}

module.exports = DynamicLoadingPage;